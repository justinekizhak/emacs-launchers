# Emacs launchers

[[_TOC_]]

<a name="unreleased"></a>

## [Unreleased]

<a name="v1.2.0"></a>

## [v1.2.0] - 2020-06-14

- Add support for Chemacs

<a name="v1.1.0"></a>

## [v1.1.0] - 2020-01-04

### Add

- Add crypto image

### Create

- Create ApexLegends edition app

### Fix

- Fix remain running bug
- Fix syntax highlighting for code blocks
- Fix broken links

### Merge

- Merge branch 'master' of gitlab.com:justinekizhak/emacs-launchers

### Move

- Move apps to SpacEmacs-Edition folder

### Update

- Update README.md

<a name="v1.0.0"></a>

## v1.0.0 - 2019-10-15

### Add

- Add documentation files
- Add license info into the script files

### Initial

- Initial commit

### Remove

- Remove a useless bash file

### Replace

- Replace everyting with better app launchers

[unreleased]: https://gitlab.com/justinekizhak/emacs-launchers/compare/v1.2.0...HEAD
[v1.2.0]: https://gitlab.com/justinekizhak/emacs-launchers/compare/v1.1.0...v1.2.0
[v1.1.0]: https://gitlab.com/justinekizhak/emacs-launchers/compare/v1.0.0...v1.1.0
