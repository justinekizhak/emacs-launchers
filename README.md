[![img](https://img.shields.io/badge/Made_in-Doom_Emacs-blue?style=for-the-badge)](https://github.com/hlissner/doom-emacs)
<a href="https://www.instagram.com/alka1e"><img src="https://i.imgur.com/G9YJUZI.png" alt="Instagram" align="right"></a>
<a href="https://twitter.com/alka1e"><img src="http://i.imgur.com/tXSoThF.png" alt="Twitter" align="right"></a>
<a href="https://www.facebook.com/justinekizhak"><img src="http://i.imgur.com/P3YfQoD.png" alt="Facebook" align="right"></a>
<br>

- - -

[![License](https://img.shields.io/badge/license-MIT-green?style=flat-square)](https://opensource.org/licenses/MIT) 

![MacOS only](https://img.shields.io/badge/platform-MacOS_only-blue?style=flat-square)

- - -

<h1> Emacs launchers </h1>

- - -

[[_TOC_]]

# Introduction

**MacOS only**

Launchers for Emacs in standard mode, Emacs in daemon mode and Emacs as client.

**[Back to top](#table-of-contents)**

# Features

- No app path required.
You don't have to add path to your emacs application into this app.

- Version agnostic
If you have installed using brew or some other methods then you know if you
upgrade the emacs version will change the application path.
This app doesn't care about that.

- Works with other Emacs applications like `Emacs-plus` etc.
As long as they can be accessed from the terminal using the standard bash
commands.

- Spacemacs icon

**[Back to top](#table-of-contents)**

# Getting Started

1. First clone or download this repo

    To clone run this on terminal

    ```sh
    git clone https://gitlab.com/justinekizhak/emacs-launchers
    ```

2. We have multiple edition of `Emacs.app` in the `build` folder.
    
    1. [SpacEmacs Edition](https://github.com/syl20bnr/spacemacs)
    2. [ApexLegends Edition](https://www.ea.com/games/apex-legends/play-now-for-free)
    

    Select which one do you want and go to that folder.  
    Then copy the `.app` folder into your `Application` directory.

3. You can launch using double-clicking or using spotlight.

**[Back to top](#table-of-contents)**

# Full Documentation

Apps included for the `SpacEmacs-Edition`:

- `Emacs.app`: Launch the full emacs version.

    This app executes a bash script called `script` with contents

    ```bash
    #!/usr/bin/env bash

    /usr/local/bin/emacs
    ```

- `Emacsserver.app`: Launch only the server. This will create a small textbox
    and print some initializing messages of emacs and the textbox closes. But there
    will be a `emacs --daemon` process running in background.

    This app executes a bash script called `script` with contents

    ```bash
    #!/usr/bin/env bash

    /usr/local/bin/emacs --daemon
    ```

- `Emacsclient.app`: To launch the Emacs "window" or the frame. Use this app.
    But when closing make sure you close only the frame not including the daemon
    instance.

    This app executes a bash script called `script` with contents

    ```bash
    #!/usr/bin/env bash

    /usr/local/bin/emacsclient -a '' -c
    ```

The `Emacsclient.app` can only launch only one frame if no frame is available.
This is because of how MacOS works. MacOS only allows one instance of app to
launch.
But to create more frames then use the command `make-frame` within emacs.
In Spacemacs its `SPC F n`.

To launch more instance of the emacsclient application itself then you can use
the terminal and run `open Emacsclient.app`.

## Developing

### Prerequisites

You will need [Platypus](https://sveinbjorn.org/platypus "Platypus"). Download
from the website.

### `src`

The source files for creating these apps are available in the `src` dir.

These are called profiles. You can open up these files in Platypus and edit them.

### Creating app from scratch

Launch Platypus then

1. Enter the name you want your app to be
2. Select script type
    
    For creating launcher icons you will need to select `Shell`.
    The default `/bin/sh` is fine for our purpose.

3. The `Script Path` will be empty initially. Click `+New` to create a shell
    script. 

    A popup will appear, here we will type our script.

    For example, to create our Emacs launcher app, delete everything inside the
    popup.
    And copy paste this
    ```
        #!/usr/bin/env bash

        /usr/local/bin/emacs
    ```
    and click `Save`
    
4. Select `Interface`
    
    For our purpose, we can select `None`. If you need to know whats being
    printed on the shell select interface `Text Window`. 
    Also you can experiment with other interfaces.

5. Check the box `Run in background`.

    This will run the app in background and there won't be any visual clutter in the MacOS dock.

6. Deselect `Remain running after execution`, because we need the app to
    just launch the main application and exit after it closes.
    Else it will keep on running.

7. Click `Create App` to finish and save to location of your choice.
    From now on you can double click to launch your application or script.
       
#### Optional parts

These steps are optional. You are to do these before clicking `Create App`.

1. Enter Identifier 
    
    Enter something along the lines of
    ```
    LASTNAME.FIRSTNAME.APPNAME
    ```
    For this app I entered
    
    ```
    kizhak.justine.Emacs
    ```
    
2. Enter name of Author
3. Enter version

## FAQ's

1. I use emacs-plus but I launch emacs-plus/something-else using separate
alias in my terminal. What are my options?

    If you want to use `Emacs-plus`, then you must make sure its can be launched by running `emacs` on terminal,

    else you have 2 options

    - Create an alias to emacs-plus and assign to `emacs` in your bash profile

    - You will have to modify the contents of the respective `script` file to
    match what you use to launch the app from your terminal.

2. Do I need to use `Emacsserver.app` everytime I start my computer?

    Not really. Unless you want to close and start the daemon instance you don't need this app.

    But you will have to make arrangements to launch emacs deamon some other way.

    I prefer using the system launcher to do that everytime I start my Mac.

    You can run this in your shell to do that.

    ```shell
    ln -sfv /usr/local/opt/emacs*/*.plist ~/Library/LaunchAgents
    launchctl load ~/Library/LaunchAgents/homebrew.mxcl.emacs*.plist
    ```

Visit [website].

Read [CHANGELOG], [CODE OF CONDUCT], [CONTRIBUTING] guide.

[CHANGELOG]: CHANGELOG.md
[CODE OF CONDUCT]: CODE_OF_CONDUCT.md
[CONTRIBUTING]: CONTRIBUTING.md

[website]: (https://justine.kizhak.com/projects/emacs-launchers)

# License

Licensed under the terms of [MIT LICENSE].

[MIT LICENSE]: LICENSE.md

**[Back to top](#table-of-contents)**

- - -

[![forthebadge](https://forthebadge.com/images/badges/makes-people-smile.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/fo-sho.svg)](https://forthebadge.com)

- - -
